<?php
/**
 * @file
 * Install, update and uninstall functions for the Mailhandler module.
 */

/**
 * Implements hook_schema().
 */
function mailhandler_schema() {
  $schema['mailhandler_mailbox'] = array(
    'description' => 'Table storing mailbox definitions',
    'fields' => array(
      'mail' => array(
        'type' => 'varchar',
        'length' => '255',
        'description' => 'Unique email address of this mailbox. Used to identify it programmatically.',
      ),
      'mid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Primary ID field for the table. Not used for anything except internal lookups.',
        'no export' => TRUE, // Do not export database-only keys.
      ),
      'settings' => array(
        'type' => 'text',
        'size' => 'big',
        'serialize' => TRUE,
        'description' => 'Configuration of mailhandler mailbox.',
      ),
    ),
    'primary key' => array('mid'),
    'unique keys' => array('mail' => array('mail'), ),
    'export' => array(
      'key' => 'mail',
      'key name' => 'Email address',
      'primary key' => 'mid',
      'identifier' => 'mailbox',
    ),
  );
  return $schema;
}

/**
 * Make 'mail' field strictly alphanumeric, to work with Features
 * http://drupal.org/node/906686
 */
function mailhandler_update_7201() {
  $ret = array();
  $result = db_query('SELECT * FROM {mailhandler_mailbox}');
  // For each mailbox, convert mail to alphanumeric, and move existing value into settings
  while ($row = $result->fetchAssoc()) {
    $row['settings'] = unserialize($row['settings']);
    $row['settings']['mail'] = $row['mail'];
    $row['mail'] = mailhandler_alphanumeric($row['mail']);
    drupal_write_record('mailhandler_mailbox', $row, 'mid');
  }
  $result = db_query('SELECT * FROM {feeds_source}');
  // For existing feed sources, need to convert selected mailboxes to alphanumeric
  while ($row = $result->fetchAssoc()) {
    $row['config'] = unserialize($row['config']);
    if (isset($row['config']['MailhandlerFetcher']['mailbox'])) {
      $row['config']['MailhandlerFetcher']['mailbox'] = mailhandler_alphanumeric($row['config']['MailhandlerFetcher']['mailbox']);
      drupal_write_record('feeds_source', $row, 'id');
    }
  }
  return $ret;
}

/**
 * Add MailhandlerCommandsFiles and MailhandlerCommandsHeaders to existing
 * Feeds importers.
 * http://drupal.org/node/1147414
 */
function mailhandler_update_7202() {
  $ret = array();
  $result = db_query('SELECT * FROM {feeds_importer}');
  while ($row = $result->fetchAssoc()) {
    $row['config'] = unserialize($row['config']);
    if ($row['config']['parser']['plugin_key'] == 'MailhandlerParser') {
      $old_plugins = $row['config']['parser']['config']['command_plugin'];
      $new_plugins = array(
        'MailhandlerCommandsFiles' => 'MailhandlerCommandsFiles',
        'MailhandlerCommandsHeaders' => 'MailhandlerCommandsHeaders',
      );
      $row['config']['parser']['config']['command_plugin'] = array_merge($old_plugins, $new_plugins);
      drupal_write_record('feeds_importer', $row, 'id');
    }
  }
  return $ret;
}

/**
 * Check that the IMAP extension exists for PHP.
 */
function mailhandler_requirements($phase) {
  // Ensure translations don't break at install time
  $t = get_t();
  $has_imap = function_exists('imap_open');

  $requirements['mailhandler'] = array(
    'title' => $t('IMAP'),
    'description' => $t("Mailhandler requires that PHP's !ext is enabled in order to function properly.", array('!ext' => l('IMAP extension', 'http://www.php.net/imap'))),
    'value' => $has_imap ? $t('Enabled') : $t('Not found'),
    'severity' => $has_imap ? REQUIREMENT_OK : REQUIREMENT_ERROR,
  );
  return $requirements;
}

function mailhandler_alphanumeric($mail) {
  $mail = drupal_strtolower(trim($mail));
  $mail = preg_replace('/[^a-z0-9-]/', '_', $mail);
  $mail = preg_replace('/-+/', "_", $mail);
  return $mail;
}

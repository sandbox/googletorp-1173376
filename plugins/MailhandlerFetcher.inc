<?php

/**
 * @file
 * Checks for new messages on an IMAP stream
 */

/**
 * Definition of the import batch object created on the fetching stage by
 * FeedsFileFetcher.
 */
class MailhandlerFetcherResult extends FeedsFetcherResult {
  protected $mailbox;
  protected $filter;

  /**
   * Constructor.
   */
  public function __construct($mailbox, $filter) {
    $this->mailbox = $mailbox;
    $this->filter = $filter;
    parent::__construct('');
  }

  /**
   * Implementation of FeedsImportBatch::getRaw();
   */
  public function getRaw() {
    $mailbox = mailhandler_mailbox_load($this->mailbox);
    module_load_include('inc', 'mailhandler', 'mailhandler.retrieve');
    if ($result = mailhandler_open_mailbox((object)$mailbox->settings)) {
      if ($new = mailhandler_get_unread_messages($result)) {
        $messages = array();
        $messages = mailhandler_retrieve($mailbox, $mailbox->settings['limit'], $mailbox->settings['encoding'], $this->filter);
        return array('messages' => $messages, 'mailbox' => $mailbox);
      }
    }
    else {
      drupal_set_message(t('Unable to connect to mailbox.'));
    }
  }
}

/**
 * Fetches data via HTTP.
 */
class MailhandlerFetcher extends FeedsFetcher {

  /**
   * Implementation of FeedsFetcher::fetch().
   */
  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    return new MailhandlerFetcherResult($source_config['mailbox'], $this->config['filter']);
  }

  /**
   * Source form.
   */
  public function sourceForm($source_config) {
    $options = array();
    $mailboxes = mailhandler_mailboxes_load();
    foreach ($mailboxes as $key => $mailbox) {
      $options[$key] = $mailbox->settings['mail'];
    }
    $form = array();
    $form['mailbox'] = array(
      '#type' => 'select',
      '#title' => t('Mailbox'),
      '#description' => t('Select a mailbox to use'),
      '#default_value' => isset($source_config['mailbox']) ? $source_config['mailbox'] : '',
      '#options' => $options,
    );
    return $form;
  }

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array(
      'filter' => 'all',
    );
  }

  /**
   * Config form.
   */
  public function configForm(&$form_state) {
    $options = array();
    $form = array();
    $filters = mailhandler_get_plugins('mailhandler', 'filters_plugin');
    foreach ($filters as $name => $filter) {
      $options[$name] = $filter['description'];
    }
    // Select message filter (to differentiate nodes/comments/etc)
    $form['filter'] = array(
      '#type' => 'select',
      '#title' => t('Message filter'),
      '#description' => t('Select which types of messages to import'),
      '#default_value' => $this->config['filter'],
      '#options' => $options,
    );
    return $form;
  }

}
